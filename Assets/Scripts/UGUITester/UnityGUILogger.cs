﻿using UnityEngine;

namespace UGUITester
{
	public static class UnityGUILogger
	{
        /// <summary>
        /// 변조된 코드에서 이 코드를 호출하여 해당 라인의 hash의 coverage를 표시함.
        /// </summary>
        /// <param name="hash"></param>
		public static void LogStatementCall(long hash)
		{
			UnityGUITester.Instance.CurrentLogData.LogStatementCall(hash);
		}
	}
}