﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UGUITester
{
    internal static class UGUICodeModifier
    {
        // Returns if this is UGUI internal or not
        public static CodeModificationResult VisitSyntaxTree(SyntaxTree tree, 
            List<SourceCodeData.StatementData> statements, Dictionary<long, SourceCodeData.StatementData> dataByHash)
        {
            UGUISyntaxRewriter rewriter = new UGUISyntaxRewriter(tree, statements, dataByHash);
            SyntaxNode newRoot = rewriter.Visit(tree.GetRoot());
            return new CodeModificationResult(rewriter.UGUIInternal, newRoot, rewriter.codeData);
        }

        private class UGUISyntaxRewriter : CSharpSyntaxRewriter
        {
            private static string ToLiteral(string input)
            {
                using (var writer = new StringWriter())
                {
                    using (var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp"))
                    {
                        provider.GenerateCodeFromExpression(new System.CodeDom.CodePrimitiveExpression(input), writer, null);
                        return writer.ToString();
                    }
                }
            }

            readonly SyntaxTree tree;
            public bool UGUIInternal { get; private set; } = false;
            public SourceCodeData codeData = new SourceCodeData();
            List<SourceCodeData.StatementData> statements;
            Dictionary<long, SourceCodeData.StatementData> dataByHash;

            public UGUISyntaxRewriter(SyntaxTree tree, List<SourceCodeData.StatementData> statements, 
                Dictionary<long, SourceCodeData.StatementData> dataByHash)
            {
                this.tree = tree;
                this.statements = statements;
                this.dataByHash = dataByHash;
            }

            public override SyntaxNode VisitNamespaceDeclaration(NamespaceDeclarationSyntax node)
            {
                if (node.Name.ToString().Contains("UGUITester"))
                {
                    UGUIInternal = true;
                    return node;
                }
                else
                {
                    return base.VisitNamespaceDeclaration(node);
                }
            }

            public override SyntaxNode Visit(SyntaxNode node)
            {
                StatementSyntax statement = node as StatementSyntax;
                if (!UGUIInternal && statement != null && !(node is BlockSyntax) && !(node is LocalDeclarationStatementSyntax))
                {
                    int line = tree.GetLineSpan(node.Span).StartLinePosition.Line - 1;
                    string statementKind = node.Kind().ToString();
                    string statementContent;
                    StringReader reader = new StringReader(node.ToFullString());
                    do
                    {
                        statementContent = reader.ReadLine().TrimStart();
                        line++;
                        if (statementContent.StartsWith("/*"))
                            while (!statementContent.Contains("*/"))
                            {
                                statementContent = reader.ReadLine().TrimStart();
                                line++;
                            }
                    }
                    while (statementContent == "" || statementContent.StartsWith("//"));
                    long hash = (long)node.GetHashCode() << 32 | (uint)statementContent.GetHashCode();

                    StatementSyntax blockCall = ExpressionStatement(
                        InvocationExpression(
                            MemberAccessExpression(
                                SyntaxKind.SimpleMemberAccessExpression,
                                MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    IdentifierName(nameof(UGUITester)),
                                    IdentifierName(nameof(UnityGUILogger))),
                                IdentifierName(nameof(UnityGUILogger.LogStatementCall))))
                        .WithArgumentList(
                            ArgumentList(
                                SingletonSeparatedList<ArgumentSyntax>(
                                    Argument(
                                        LiteralExpression(
                                            SyntaxKind.NumericLiteralExpression,
                                            Literal(hash)))))))
						 .WithSemicolonToken(
							Token(
								TriviaList(),
								SyntaxKind.SemicolonToken,
								TriviaList(
									CarriageReturnLineFeed)));

                    while (dataByHash.ContainsKey(hash))
                    {
                        if (hash == long.MaxValue) hash = long.MinValue;
                        else hash++;
                    }
                    var data = new SourceCodeData.StatementData(line, statementContent, statementKind, hash);
                    statements.Add(data);
                    dataByHash.Add(hash, data);
                    
                    return Block(blockCall, (StatementSyntax)base.Visit(statement));
                }
                else return base.Visit(node);
                //IfStatementSyntax iif; iif.
            }
        }
    }
}