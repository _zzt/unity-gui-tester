﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UGUITester
{
    struct TestableActionTree
    {
        TestableActionNode root;

        readonly int maxlife;

        public TestableActionTree(TestableActionNode rootNode, int maxlife = 1)
        {
            root = rootNode;
            this.maxlife = maxlife;
        }

        //public TestableActionTree(TestableAction? action)
        //{
        //    root = new TestableActionNode(action, null);
        //}

        public TestableActionTree(List<ExecutionLogData> previousLogData, List<TestableAction> testableActions, int maxlife)
        {
            this.maxlife = maxlife;
            int i;
            bool b;
            root = new TestableActionNode(null, null, maxlife);

            if (root.isChildEmpty())
            {
                foreach (var testableAction in testableActions)
                {
                    root.AddChild(testableAction);
                }
            }

            TestableActionNode node = root;
            foreach (ExecutionLogData logData in previousLogData)
            {
                i = 1;
                node = root;
                foreach (var executedAction in logData.ExecutedActionsByTime)
                {
                    if (node.isChildEmpty())
                    {
                        foreach (var testableAction in testableActions)
                        {
                            node.AddChild(testableAction);
                        }
                    }

                    b = false;
                    //Debug.Log(executedAction.Value.MousePos);
                    foreach (var child in node.childs)
                    {
                        //Debug.Log(child.action.Value.WorldPos);
                        //Debug.Log(child.action.Value.MousePos);
                        //if (child.action.Value.Equals(executedAction.Value))
                        if (child.action.Value.WorldPos == executedAction.Value.WorldPos)
                        {
                            b = true;
                            node = child;
                            if (node.isChildEmpty())
                            {
                                foreach (var testableAction in testableActions)
                                {
                                    node.AddChild(testableAction);
                                }
                            }
                            break;
                        }
                    }
                    if (!b)
                    {
                        break;
                    }
                    if (logData.CoverageChanged[i] == 0)
                    {
                        node.Life = Math.Min(node.parent.Life - 1, node.Life);
                    }
                    i++;
                }
            }
        }

        public List<TestableActionNode> MakeTestSequence()
        {
            TestableActionNode node = root;

            List<TestableActionNode> sequence = new List<TestableActionNode>();

            if (node != null && node.Life > 0)
            {
                node = node.ChooseNextActionNode(sequence);
            }

            while (node != null && node.Life > 0)
            {
                sequence.Add(node);
                node = node.ChooseNextActionNode(sequence);
            }
            return sequence;
        }
    }

    class TestableActionNode
    {
        public TestableActionNode parent;
        public TestableAction? action;
        public List<TestableActionNode> childs;

        private int life;
        public int Life
        {
            get
            {
                return life;
            }
            set
            {
                life = value;
                if (value <= 0)
                {
                    if (parent != null)
                    {
                        parent.updateLife();
                    }
                }
            }
        }

        public TestableActionNode(TestableAction? action, TestableActionNode parent, int life)
        {
            this.action = action;
            this.parent = parent;
            childs = new List<TestableActionNode>();
            Life = life;
        }

        public bool isChildEmpty()
        {
            return childs.Count == 0;
        }

        public void AddChild(TestableActionNode child)
        {
            child.parent = this;
            childs.Add(child);
        }

        public void AddChild(TestableAction action)
        {
            TestableActionNode child = new TestableActionNode(action, this, Life);
            child.parent = this;
            childs.Add(child);
        }

        public void Execute()
        {
            if (Life > 0)
            {
                if (action != null)
                {
                    int prevCoverage = UnityGUITester.Instance.CurrentLogData.CoveredStatements.Count;
                    action.Value.Execute();
                    int currentCoverage = UnityGUITester.Instance.CurrentLogData.CoveredStatements.Count;
                    //if (prevCoverage == currentCoverage)
                    //{
                    //    Life--;
                    //}
                    //else
                    //{
                    //    Life = maxLife;
                    //}
                }
            }
        }

        public void ExecuteNextAll()
        {
            foreach (var node in childs)
            {
                node.Execute();
            }
        }

        public void ExecuteNext(int index)
        {
            if (index >= 0 && index < childs.Count)
            {
                childs[index].Execute();
            }
        }

        public TestableActionNode ExecuteNext()
        {
            int max = 0;
            TestableActionNode nextNode = null;
            foreach (TestableActionNode node in childs)
            {
                if (node.Life > max)
                {
                    max = node.Life;
                    nextNode = node;
                }
            }
            if (nextNode != null)
            {
                nextNode.Execute();
            }
            return nextNode;
        }

        public TestableActionNode ChooseNextActionNode(List<TestableActionNode> seq)
        {
            int max = -1;
            int priority;
            TestableActionNode nextNode = null;
            List<TestableActionNode> nodeList = new List<TestableActionNode>();
            foreach (TestableActionNode node in childs)
            {
                priority = node.Life + seq.Count;
                foreach(TestableActionNode n in seq)
                {
                    if(n.action.Value.GetMethodInfoStr() == node.action.Value.GetMethodInfoStr())
                    {
                        priority--;
                    }
                }
                if (priority > max && node.Life > 0)
                {
                    max = node.Life;
                    nextNode = node;
                }
            }
            return nextNode;
        }

        private void updateLife()
        {
            foreach (var child in childs)
            {
                if (child.Life > 0)
                {
                    return;
                }
            }
            Life = 0;
        }
    }
}

