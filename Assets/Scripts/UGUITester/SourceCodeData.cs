﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace UGUITester
{
	[Serializable]
	public class SourceCodeData
	{
		[Serializable]
		public struct StatementData
		{
			public int line;
			public string content;
			public string kind;
			public long hash;

			[JsonConstructor]
			public StatementData(int line, string content, string kind, long hash)
			{
				this.line = line;
				this.content = content;
				this.kind = kind;
				this.hash = hash;
			}
		}

		public Dictionary<string, List<StatementData>> DataByFile = new Dictionary<string, List<StatementData>>();

		public SourceCodeData()
		{
		}

		[JsonConstructor]
		public SourceCodeData(Dictionary<string, List<StatementData>> dataByFile)
		{
			DataByFile = dataByFile;
			foreach(var statementDatas in dataByFile.Values)
				foreach(StatementData stmtData in statementDatas)
				{
					Debug.Assert(!DataByHash.ContainsKey(stmtData.hash), "OOPS, Hash Collision!!!");
					DataByHash[stmtData.hash] = stmtData;
				}
			
		}

		[NonSerialized]
		public Dictionary<long, StatementData> DataByHash = new Dictionary<long, StatementData>();
	}
}