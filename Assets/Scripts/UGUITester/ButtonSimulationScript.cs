﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UGUITester
{
	public class ButtonSimulationScript : MonoBehaviour
	{
		public static ButtonSimulationScript Instance { get; private set; }

		// Use this for initialization
		void Start()
		{
			Debug.Assert(Instance == null, "ButtonSimulatescript Singleton duplicate");
			Instance = this;
			StartCoroutine(AutoClick());
		}

		private void OnDestroy()
		{
			Debug.Assert(Instance == this);
			Instance = null;
		}

		Dictionary<Type, MethodInfo> MethodDic = new Dictionary<Type, MethodInfo>()
			{{typeof(IPointerClickHandler), typeof(IPointerClickHandler).GetMethod("OnPointerClick") },
			{typeof(ISubmitHandler), typeof(ISubmitHandler).GetMethod("OnSubmit") },
			{typeof(ICancelHandler), typeof(ICancelHandler).GetMethod("OnCancel") },
			{typeof(IMoveHandler), typeof(IMoveHandler).GetMethod("OnMove") },
			{typeof(IPointerDownHandler), typeof(IPointerDownHandler).GetMethod("OnPointerDown") },
			{typeof(IPointerUpHandler), typeof(IPointerUpHandler).GetMethod("OnPointerUp") },
			{typeof(IPointerEnterHandler), typeof(IPointerEnterHandler).GetMethod("OnPointerEnter") },
			{typeof(IPointerExitHandler), typeof(IPointerExitHandler).GetMethod("OnPointerExit") },
			{typeof(ISelectHandler), typeof(ISelectHandler).GetMethod("OnSelect") },
			{typeof(IDeselectHandler), typeof(IDeselectHandler).GetMethod("OnDeselect") },
			{typeof(IBeginDragHandler), typeof(IBeginDragHandler).GetMethod("OnBeginDrag") },
			{typeof(IDragHandler), typeof(IDragHandler).GetMethod("OnDrag") },
			{typeof(IEndDragHandler), typeof(IEndDragHandler).GetMethod("OnEndDrag") },
			{typeof(IInitializePotentialDragHandler), typeof(IInitializePotentialDragHandler).GetMethod("OnInitializePotentialDrag") },
			{typeof(IScrollHandler), typeof(IScrollHandler).GetMethod("OnScroll") }, };

		public object[] GetArgs(Type t, GameObject obj)
		{
			if (t == typeof(IPointerClickHandler) || t == typeof(ISubmitHandler) || t == typeof(ICancelHandler)
				|| t == typeof(IPointerDownHandler) || t == typeof(IPointerUpHandler))
			{
				return new object[] {
					new PointerEventData(FindObjectOfType<EventSystem>())
					{
						position = obj.transform.position,
						button = PointerEventData.InputButton.Left,
					}};
			}
			else if (t == typeof(IMoveHandler))
			{
				return new object[] {
					new AxisEventData(FindObjectOfType<EventSystem>())
					{
						moveDir = MoveDirection.Down,
					}};
			}

			return null;
		}

		public void InvokeHandler(GameObject obj, Type t)
		{
			MethodInfo me = MethodDic[t];

			me.Invoke(obj.GetComponent(t), GetArgs(t, obj));
		}

		public void ClickObject(GameObject obj)
		{
			EventSystem system = FindObjectOfType<EventSystem>();
			StandaloneInputModule module = FindObjectOfType<StandaloneInputModule>();

			PointerEventData eventData = new PointerEventData(system)
			{
				pointerCurrentRaycast = new RaycastResult()
				{
					gameObject = obj,
				},
				eligibleForClick = true,
				button = PointerEventData.InputButton.Left,
				position = gameObject.transform.position,
			};

			ExecuteEvents.EventFunction<IPointerClickHandler> callback = (handler, ed) =>
			{
				handler.OnPointerClick(ExecuteEvents.ValidateEventData<PointerEventData>(ed));
			};

			ExecuteEvents.ExecuteHierarchy<IPointerClickHandler>(obj, eventData, callback);
		}

		public void DragObject(GameObject obj)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("Object name is {0}\n", obj);
			foreach (Type t in typeof(IEventSystemHandler).Assembly.GetTypes().Where(t => typeof(IEventSystemHandler).IsAssignableFrom(t)))
			{
				sb.AppendLine(t.Name);

				foreach (Component c in obj.GetComponents<Component>())
					sb.AppendFormat("\tComponent {0}: {1}\n",
						c.GetType().Name.PadRight(20),
						typeof(ExecuteEvents).GetMethod("ShouldSendToComponent", BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(t).Invoke(null, new object[] { c }));

				sb.AppendLine();
				//sb.AppendFormat("{0} {1}\n", t.Name, typeof(ExecuteEvents).GetMethod("CanHandleEvent").MakeGenericMethod(t).Invoke(null, new object[] { obj }));
			}

			Debug.Log(sb);


			EventSystem system = FindObjectOfType<EventSystem>();
			StandaloneInputModule module = FindObjectOfType<StandaloneInputModule>();

			PointerEventData eventData = new PointerEventData(system)
			{
				pointerCurrentRaycast = new RaycastResult()
				{
					gameObject = obj,
				},
				dragging = true,
				eligibleForClick = true,
				button = PointerEventData.InputButton.Left,
				position = obj.transform.position,
				pressPosition = obj.transform.position + Vector3.down,
				pointerDrag = obj,
			};

			ExecuteEvents.EventFunction<IDragHandler> callback = (handler, ed) =>
			{
				handler.OnDrag(ExecuteEvents.ValidateEventData<PointerEventData>(ed));
			};

			ExecuteEvents.ExecuteHierarchy<IDragHandler>(obj, eventData, callback);


		}

		public void ClickPosition(Vector2 screenPos)
		{
			EventSystem system = FindObjectOfType<EventSystem>();

			PointerEventData eventData = new PointerEventData(system)
			{
				position = screenPos,
			};

			Debug.Log("ClickPos : " + screenPos);

			List<RaycastResult> raycastList = new List<RaycastResult>();
			system.RaycastAll(eventData, raycastList);

			var a = Physics2D.OverlapPointAll(Camera.main.ScreenToWorldPoint(screenPos));


			bool clicked = false;

			foreach (var b in a)
			{
				if (clicked)
				{
					return;
				}

				Debug.Log(b.gameObject);

				if (b.gameObject != null)
				{
					b.gameObject.SendMessage("OnMouseDown");
					clicked = true;
					break;
				}
			}

			//Debug.Log(raycastList.Count);

			foreach (RaycastResult r in raycastList)
			{
				if (clicked)
				{
					return;
				}

				Debug.Log(r);

				if (r.gameObject != null)
				{
					ClickObject(r.gameObject);
					clicked = true;
					break;
				}
			}
		}

		IEnumerator AutoClick()
		{
			// 1초 후 전방에 클릭 발싸!
			yield return new WaitForSeconds(1);

			GameObject target = FindObjectOfType<Text>().gameObject;

			// 둘 중 하나를 주석 해제해서 쓰시오
			// 하나는 GameObject에 날리는 것, 하나는 좌표에 날리는 것.

			// ClickObject(target);
			// ClickPosition(target.transform.position);
		}

		public void Yee()
		{
			Debug.Log("Yee");

			FindObjectOfType<Text>().text = "YEE";
		}
	}

}