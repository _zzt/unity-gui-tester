﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace UGUITester
{
	internal enum ActionType
	{
		MouseClick,
		MouseOver,
	}

	[Serializable]
	internal struct TestableAction
	{
		private static readonly Dictionary<Type, String> miDic = new Dictionary<Type, string>()
		{
			{typeof(Button), "Press" },
			{typeof(Dropdown), "Show" },
			{typeof(Scrollbar), "Set" },
			{typeof(Slider), "Set" },
			{typeof(Toggle), "Set" },
		};

		public void Execute()
		{
            if (UnityGUITester.Instance.IsCodeModified)
            {
                UnityGUITester.Instance.CurrentLogData.SaveCurrentCoverage();
                UnityGUITester.Instance.CurrentLogData.ExecutedActionsByTime.Add(Time.time, this);
            }
			if (type == typeof(Button))
			{
				ConnectedObject.GetComponent(type).InvokeMethod(miDic[type]);
			}
			else if (type == typeof(Dropdown))
			{
				ConnectedObject.GetComponent(type).InvokeMethod(miDic[type]);
			}
			else if (type == typeof(Scrollbar))
			{
				ConnectedObject.GetComponent(type).InvokeMethod(miDic[type], (float)1);
			}
			else if (type == typeof(Slider))
			{
				ConnectedObject.GetComponent(type).InvokeMethod(miDic[type], (float)1);
			}
			else if (type == typeof(Toggle))
			{
				ConnectedObject.GetComponent(type).InvokeMethod(miDic[type], (bool)true);
			}
		}

		// https://answers.unity.com/questions/956047/serialize-quaternion-or-vector3.html
		[Serializable]
		struct SerializableVector3
		{
			/// <summary>
			/// x component
			/// </summary>
			public float x;

			/// <summary>
			/// y component
			/// </summary>
			public float y;

			/// <summary>
			/// z component
			/// </summary>
			public float z;

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="rX"></param>
			/// <param name="rY"></param>
			/// <param name="rZ"></param>
			public SerializableVector3(float rX, float rY, float rZ)
			{
				x = rX;
				y = rY;
				z = rZ;
			}

			/// <summary>
			/// Returns a string representation of the object
			/// </summary>
			/// <returns></returns>
			public override string ToString()
			{
				return String.Format("[{0}, {1}, {2}]", x, y, z);
			}

			/// <summary>
			/// Automatic conversion from SerializableVector3 to Vector3
			/// </summary>
			/// <param name="rValue"></param>
			/// <returns></returns>
			public static implicit operator Vector3(SerializableVector3 rValue)
			{
				return new Vector3(rValue.x, rValue.y, rValue.z);
			}

			/// <summary>
			/// Automatic conversion from Vector3 to SerializableVector3
			/// </summary>
			/// <param name="rValue"></param>
			/// <returns></returns>
			public static implicit operator SerializableVector3(Vector3 rValue)
			{
				return new SerializableVector3(rValue.x, rValue.y, rValue.z);
			}
		}

		[JsonProperty]
		private SerializableVector3 worldPos;
		public object UIInputParameter;
		public readonly Type type;
		[JsonProperty]
		private string ConnectedObjectName { get { return ConnectedObject.name; } }
		[JsonIgnore]
		public Vector3 WorldPos { get { return worldPos; } }
		[JsonIgnore]
		public readonly MethodInfo Method;
        [JsonProperty]
        public readonly string MethodInfoString;
        [JsonProperty]
        private string MethodInformation { get { return Method.ToString(); } }
		[JsonIgnore]
		public readonly GameObject ConnectedObject;
		[JsonIgnore]
		public Vector2 MousePos { get { return Camera.main.WorldToScreenPoint(WorldPos); } }

		/// <summary>
		/// 1. 버튼의 어디쯤 클릭해야 하는지 정보
		/// 2. 어떤 액션인지 (클릭 가따데기 등)
		/// 3. 게임오브젝트
		/// </summary>
		/// <param name="worldPos"></param>
		/// <param name="actionType"></param>
		/// <param name="connectedObject"></param>
		public TestableAction(Vector3 worldPos, GameObject connectedObject, Type t, MethodInfo method)
			: this((SerializableVector3)worldPos, connectedObject, t, method) { }

		[JsonConstructor]
        private TestableAction(SerializableVector3 worldPos, Type t, string MethodInfoString) : this(worldPos, null, t, null, MethodInfoString) { }
        //private TestableAction(SerializableVector3 worldPos, Type t) : this(worldPos, null, t, null) { }

        private TestableAction(SerializableVector3 worldPos, GameObject connectedObject, Type t, MethodInfo method, object UIInputParameter = null)
		{
			this.worldPos = worldPos;
			ConnectedObject = connectedObject;
			type = t;
			Method = method;
			this.UIInputParameter = UIInputParameter;
            MethodInfoString = method.ToString() + t.ToString();
		}

        private TestableAction(SerializableVector3 worldPos, GameObject connectedObject, Type t, MethodInfo method, string MethodInfoString, object UIInputParameter = null)
        {
            this.worldPos = worldPos;
            ConnectedObject = connectedObject;
            type = t;
            Method = method;
            this.UIInputParameter = UIInputParameter;
            this.MethodInfoString = MethodInfoString;
        }

        public override bool Equals(object obj)
		{
			if (!(obj is TestableAction)) return false;
			TestableAction action = (TestableAction)obj;
			return type == action.type && WorldPos == action.WorldPos; // Vector3 == 은 실수 오차를 잘 고려함
            
		}

		public override int GetHashCode()
		{
			var hashCode = 948776744; // VS가 이 숫자 다 자동생성해 줌
			hashCode = hashCode * -1521134295 + EqualityComparer<Vector3>.Default.GetHashCode(WorldPos);
			hashCode = hashCode * -1521134295 + EqualityComparer<Type>.Default.GetHashCode(type);
			return hashCode;
		}

        public string GetMethodInfoStr()
        {
            return Method.ToString() + type.ToString();
        }
	}
}