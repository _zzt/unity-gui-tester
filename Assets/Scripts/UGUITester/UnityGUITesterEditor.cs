﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace UGUITester
{
	public partial class UnityGUITester : MonoBehaviour
	{
		[CustomEditor(typeof(UnityGUITester))]
		public class UnityGUITesterEditor : Editor
		{
			void SetExecutionLogPath()
			{
				UnityGUITester tester = (UnityGUITester)target;
				tester.ExecutionLogPath =
					EditorUtility.SaveFolderPanel("Set Log Path", "ExecutionLogs", tester.ExecutionLogPath) + "/";
			}

			public override void OnInspectorGUI()
			{
				UnityGUITester tester = (UnityGUITester)target;

				if (Application.isPlaying)
				{
					GUILayout.Box("Current Target Path:\n" + tester.TargetPath);
					GUILayout.Box("Current Restore Path:\n" + tester.RestorePath);
					GUILayout.Box("Current Execution Log Filename:\n" + tester.ExecutionLogPath);
					if (GUILayout.Button("Set Execution Log Filename"))
						SetExecutionLogPath();

					if (UnityGUITester.Instance.IsCodeModified && GUILayout.Button("Write Execution Log"))
						UnityGUITester.Instance.WriteExecutionLog();

					if (GUILayout.Button("Stop the Game"))
					{
						tester.true_stop = true;
						EditorApplication.isPaused = false;
						EditorApplication.isPlaying = false;
					}
				}
				else
				{
					base.OnInspectorGUI();

					if (GUILayout.Button("Generate Seed"))
						tester.Seed = System.Environment.TickCount;

					if (GUILayout.Button("Set Target Path"))
						tester.TargetPath = EditorUtility.OpenFolderPanel("Set Target Path", "Assets", tester.TargetPath) + "/";

					if (GUILayout.Button("Set Restore Path"))
						tester.RestorePath = EditorUtility.SaveFolderPanel("Set Restore Path", "Backup", tester.RestorePath) + "/";

					if (GUILayout.Button("Set Execution Log Path"))
						SetExecutionLogPath();

					if (GUILayout.Button("Delete Existing Execution Logs"))
						tester.RemoveExecutionLog();

					if (GUILayout.Button("Rewrite Code"))
						tester.RewriteCode();

					if (GUILayout.Button("Restore Code"))
						tester.RestoreCode();
				}
			}
		}
	}
}

#endif