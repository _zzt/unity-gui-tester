﻿using Microsoft.CodeAnalysis;

namespace UGUITester
{
	internal struct CodeModificationResult
	{
		public bool UGUIinternal;
		public SyntaxNode newRoot;
		public SourceCodeData coverageData;

		public CodeModificationResult(bool uGUIinternal, SyntaxNode newRoot, SourceCodeData coverageData)
		{
			UGUIinternal = uGUIinternal;
			this.newRoot = newRoot;
			this.coverageData = coverageData;
		}
	}
}