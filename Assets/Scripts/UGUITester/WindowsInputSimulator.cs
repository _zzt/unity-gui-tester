﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
using System.Runtime.InteropServices;
using System;

namespace UGUITester
{
    public enum ScanCode : byte
    {
        VK_LBUTTON = 1,
        VK_RBUTTON,
        VK_MBUTTON = 4,
        KEY_0 = 48,
        KEY_1,
        KEY_2,
        KEY_3,
        KEY_4,
        KEY_5,
        KEY_6,
        KEY_7,
        KEY_8,
        KEY_9,
        KEY_A = 65,
        KEY_B,
        KEY_C,
        KEY_D,
        KEY_E,
        KEY_F,
        KEY_G,
        KEY_H,
        KEY_I,
        KEY_J,
        KEY_K,
        KEY_L,
        KEY_M,
        KEY_N,
        KEY_O,
        KEY_P,
        KEY_Q,
        KEY_R,
        KEY_S,
        KEY_T,
        KEY_U,
        KEY_V,
        KEY_W,
        KEY_X,
        KEY_Y,
        KEY_Z,
        VK_NUMLOCK = 90
    }

    //Mouse actions
    [Flags]
    public enum MouseEvent
    {
        MOUSEEVENTF_LEFTDOWN = 0x02,
        MOUSEEVENTF_LEFTUP = 0x04,
        MOUSEEVENTF_RIGHTDOWN = 0x08,
        MOUSEEVENTF_RIGHTUP = 0x10
    }

    public static class WindowsInputSimulator
    {
        

        [DllImport("user32.dll")]
        private static extern bool SetCursorPos(int X, int Y);
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out Point pos);

        const uint KEYEVENTF_EXTENDEDKEY = 0x0001;
        const uint KEYEVENTF_KEYUP = 0x0002;

        [DllImport("user32.dll")]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);

        public static void KeyDown(ScanCode keyCode)
        {
            KeyDown((byte)keyCode);
        }

        public static void KeyDown(byte keyCode)
        {
            keybd_event(keyCode, 0x45, KEYEVENTF_EXTENDEDKEY, 0);

        }

        public static void KeyUp(ScanCode keyCode)
        {
            KeyUp((byte)keyCode);
        }

        public static void KeyUp(byte keyCode)
        {
            keybd_event(keyCode, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }

        public struct Point
        {
            public Int32 x;
            public Int32 y;

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public static Point UnityMouseCoordToPoint(Vector2 mousePos)
        {
            Vector2 currentmPos = Input.mousePosition;
            Point mPos_pt;
            GetCursorPos(out mPos_pt);
            Vector2 diff = mousePos - currentmPos;
            Debug.Log(currentmPos);
            Debug.Log(mousePos);
            Debug.Log(diff);
            Debug.Log(new Vector2(mPos_pt.x, mPos_pt.y));
            return new Point(mPos_pt.x + (int)diff.x, mPos_pt.y - (int)diff.y);
        }

        public static void MoveCursorToScreenPosition(int x, int y)
        {
            MoveCursorToScreenPosition(new Vector2(x, y));
        }

        public static void MoveCursorToScreenPosition(Vector2 newPos)
        {
            Vector2 mPos = Input.mousePosition;
			Vector2 diff = newPos - mPos;
			Point mPos_pt;
            GetCursorPos(out mPos_pt);
            SetCursorPos(mPos_pt.x + (int)diff.x, mPos_pt.y - (int)diff.y);
        }

        [DllImport("user32.dll")]
        private static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        public static void MouseEvent(MouseEvent mouseEvent)
        {
            Point mPos_pt;
            GetCursorPos(out mPos_pt);
            MouseEvent(mouseEvent, mPos_pt.x, mPos_pt.y);
        }

        public static void MouseEvent(MouseEvent mouseEvent, int x, int y)
        {
            mouse_event((uint)mouseEvent, (uint)x, (uint)y, 0, 0);
        }
    }
}

#endif