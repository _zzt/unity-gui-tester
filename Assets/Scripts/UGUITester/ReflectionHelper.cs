﻿using System;
using System.Linq;
using System.Reflection;

namespace UGUITester
{

	public static class ReflectionHelper
	{
		private static readonly BindingFlags InstanceFlag;
		private static readonly BindingFlags StaticFlag;

		static ReflectionHelper()
		{
			InstanceFlag = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			StaticFlag = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
		}

		public static Type GetTypeInAppdomain(String fullName)
		{
			foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
				foreach (Type t in asm.GetTypes())
					if (t.FullName == fullName)
						return t;

			return null;
		}

		public static Object InvokeStaticMethod(this Type t, String name, params Object[] args)
		{
			foreach (MethodInfo mi in t.GetType().GetMethods(StaticFlag))
				if (mi.Name == name &&
					mi.GetGenericArguments().Length == args.Length &&
					Enumerable.Range(0, args.Length).All(idx => mi.GetGenericArguments()[idx].DeclaringType.IsAssignableFrom(args[idx].GetType())))
					return mi.Invoke(null, args);

			throw new ArgumentException("Wrong parameter setting");
		}

		public static Object GetField(this Object obj, String name)
		{
			Type t = obj.GetType();
			do
			{
				FieldInfo fi = t.GetField(name, InstanceFlag);
				if (fi != null)
					return fi.GetValue(obj);

				t = t.BaseType;
			}
			while (t != typeof(Object));

			throw new ArgumentException("Wrong parameter setting");
		}

		public static MethodInfo GetMethodInfo(this Object obj, String name)
		{
			return obj.GetType().GetMethod(name, InstanceFlag);
		}

		public static Object GetProperty(this Object obj, String name)
		{
			return obj.GetType().GetProperty(name, InstanceFlag).GetValue(obj, null);
		}

		public static Object InvokeIndexer(this Object obj, String name, params Object[] indices)
		{
			return obj.GetType().GetProperty(name, InstanceFlag).GetValue(obj, indices);
		}

		public static Object InvokeMethod(this Object obj, String name, params Object[] args)
		{
			Type t = obj.GetType();
			do
			{
				foreach (MethodInfo mi in obj.GetType().GetMethods(InstanceFlag))
					if (mi.Name == name &&
						mi.GetParameters().Length == args.Length &&
						Enumerable.Range(0, args.Length).All(idx => mi.GetParameters()[idx].ParameterType.IsAssignableFrom(args[idx].GetType())))
						return mi.Invoke(obj, args);

				t = t.BaseType;
			}
			while (t != typeof(Object));

			throw new ArgumentException("Wrong parameter setting");
		}
	        
    }
}