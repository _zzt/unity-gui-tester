﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Reflection;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json;
using System.IO;
using UnityEngine.EventSystems;

namespace UGUITester
{
    public partial class UnityGUITester : MonoBehaviour
    {
        public static UnityGUITester Instance { get; private set; }

        public bool FixSeed;
        public int Seed = 201400367;
        public float WaitTerm = 5.0f;
        public int TestMaxLife = 1;

        public float TimeOut = 60f;

        public enum SampleType
        {
            CanvasRandom,
            ObjectRandom,
            TreeAlgorithm,
        }

        public SampleType sampleType = SampleType.CanvasRandom;

        public bool IsCodeModified { get; private set; } = false;

        private void Awake()
        {
            Debug.Assert(Instance == null, "Assertion Failed: UnityGUITester already exists!");
            Instance = this;

            UnityEngine.Random.InitState(Seed);
            ExtractActions();

            IsCodeModified = Directory.Exists(RestorePath) && File.Exists(RestorePath + codeDataFilename);
            if (!IsCodeModified)
            {
                Debug.Log("Unity GUI Tester couldn't find code modification data. Assuming that this code is not modified.");
                return;
            }

            CurrentCodeData = JsonConvert.DeserializeObject<SourceCodeData>(File.ReadAllText(RestorePath + codeDataFilename));
            CurrentLogData = new ExecutionLogData(CurrentCodeData, int.Parse(DateTime.Now.ToString("MMddhhmmss")));

            Directory.CreateDirectory(ExecutionLogPath);

            foreach (string sourceFileName in Directory.EnumerateFiles(ExecutionLogPath, "*.json", SearchOption.AllDirectories))
            {
                PreviousLogData.Add(JsonConvert.DeserializeObject<ExecutionLogData>(File.ReadAllText(sourceFileName)));
            }

            testableActiontree = new TestableActionTree(PreviousLogData, testableActions, TestMaxLife);
            testSequence = testableActiontree.MakeTestSequence();
        }

        public float ActionFrequency = 0.1f;

        public string TargetPath = "Assets/";
        public string RestorePath = "Backup/";
        public string ExecutionLogPath = "ExecutionLogs/";

        const string codeDataFilename = "Code Data.json";

        [HideInInspector]
        public List<ExecutionLogData> PreviousLogData = new List<ExecutionLogData>();
        [HideInInspector]
        public ExecutionLogData CurrentLogData;
        [HideInInspector]
        public SourceCodeData CurrentCodeData;

        private static List<TestableAction> testableActions = new List<TestableAction>();
        private static List<TestableInterface> testableInterfaces = new List<TestableInterface>();

        private TestableActionTree testableActiontree = new TestableActionTree(null);
        private List<TestableActionNode> testSequence = new List<TestableActionNode>();

        // Use this for initialization
        IEnumerator Start()
        {
            if (!IsCodeModified) yield break; // yield break은 종료같은 역할을 함

            if (sampleType == SampleType.TreeAlgorithm)
            {
                if (testSequence.Count > 0)
                {
                    foreach (var a in testSequence)
                    {
                        ExtractActions();
                        yield return new WaitForSeconds(ActionFrequency);
                        if (a.action.Value.ConnectedObject == null)
                        {
                            testableActions.Remove(a.action.Value);
                        }
                        else
                        {
                            a.Execute();
                        }
                    }
                    Debug.Log("Execution of test " + CurrentLogData.ID + " finished, shutting down in " + WaitTerm + " seconds.");
                    yield return new WaitForSeconds(WaitTerm);
                    UnityEditor.EditorApplication.isPlaying = false;
                    yield return null;
                }
                else
                {
                    Debug.Log("Automated test finished, shutting down in " + WaitTerm + " seconds.");
                    yield return new WaitForSeconds(WaitTerm);
                    true_stop = true;
                    UnityEditor.EditorApplication.isPlaying = false;
                    yield return null;
                }
            }
            else if (sampleType == SampleType.CanvasRandom)
            {
                CurrentLogData = new ExecutionLogData(CurrentCodeData, int.Parse(DateTime.Now.ToString("MMddhhmmss")));

                true_stop = true;

                while (TimeOut > 0)
                {
                    TimeOut -= 0.1f;
                    yield return new WaitForSeconds(WaitTerm);
                    ButtonSimulOne();
                }
                UnityEditor.EditorApplication.isPlaying = false;
            }
            else if (sampleType == SampleType.ObjectRandom)
            {
                CurrentLogData = new ExecutionLogData(CurrentCodeData, int.Parse(DateTime.Now.ToString("MMddhhmmss")));

                true_stop = true;

                while (TimeOut > 0)
                {
                    TimeOut -= WaitTerm;
                    ExtractActions();
                    yield return new WaitForSeconds(WaitTerm);
                    ButtonSimulTwo();
                }
                UnityEditor.EditorApplication.isPlaying = false;
            }
        }

        private void Update()
        {

        }

        private static Dictionary<string, ActionType> monobehaviourFunctionLink = new Dictionary<string, ActionType>()
        {
            { "OnMouseUpAsButton", ActionType.MouseClick },
            { "OnMouseDown", ActionType.MouseClick },
            { "OnMouseDrag", ActionType.MouseClick },
            { "OnMouseEnter", ActionType.MouseClick },
            { "OnMouseExit", ActionType.MouseClick },
            { "OnMouseOver", ActionType.MouseClick },
            { "OnMouseUp", ActionType.MouseClick },
        };

        private static List<Type> InterfaceList = new List<Type>()
        {
            typeof(IPointerClickHandler),
            typeof(ISubmitHandler),
            typeof(ICancelHandler),
            typeof(IMoveHandler),
            typeof(IPointerDownHandler),
            typeof(IPointerUpHandler),
            typeof(IPointerEnterHandler),
            typeof(IPointerExitHandler),
            typeof(ISelectHandler),
            typeof(IDeselectHandler),
            typeof(IBeginDragHandler),
            typeof(IDragHandler),
            typeof(IEndDragHandler),
            typeof(IInitializePotentialDragHandler),
            typeof(IScrollHandler),
        };

        void ExtractActions()
        {
            HierarchyInspector.TraverseHierarchy(gameObject =>
            {
                CatchUIToAction(gameObject);
            });
        }

        void CatchUIToAction(GameObject obj)
        {
            Button button = obj.GetComponent<Button>();
            if (button != null)
            {
                Vector3? collisionCenter = null;
                collisionCenter = button.transform.position;
                Type type = button.GetType();

                foreach (MethodInfo mi in HierarchyInspector.GetButtonPersistentMethodInfo(button))
                {
                    AddToAction(new TestableAction(collisionCenter.Value, obj, type, mi));
                }
                return;
            }
            Dropdown dropdown = obj.GetComponent<Dropdown>();
            if (dropdown != null)
            {
                Vector3? collisionCenter = null;
                collisionCenter = dropdown.transform.position;
                Type type = dropdown.GetType();

                foreach (MethodInfo mi in HierarchyInspector.GetDropdownPersistentMethodInfo(dropdown))
                {

                    AddToAction(new TestableAction(collisionCenter.Value, obj, type, mi));
                }
                return;
            }
            Scrollbar scroll = obj.GetComponent<Scrollbar>();
            if (scroll != null)
            {
                Vector3? collisionCenter = null;
                collisionCenter = scroll.transform.position;
                Type type = scroll.GetType();

                foreach (MethodInfo mi in HierarchyInspector.GetScrollbarPersistentMethodInfo(scroll))
                {
                    AddToAction(new TestableAction(collisionCenter.Value, obj, type, mi));
                }
                return;
            }
            Slider slider = obj.GetComponent<Slider>();
            if (slider != null)
            {
                Vector3? collisionCenter = null;
                collisionCenter = slider.transform.position;
                Type type = slider.GetType();

                foreach (MethodInfo mi in HierarchyInspector.GetSliderPersistentMethodInfo(slider))
                {
                    AddToAction(new TestableAction(collisionCenter.Value, obj, type, mi));
                }
                return;
            }
            Toggle toggle = obj.GetComponent<Toggle>();
            if (toggle != null)
            {
                Vector3? collisionCenter = null;
                collisionCenter = toggle.transform.position;
                Type type = toggle.GetType();

                foreach (MethodInfo mi in HierarchyInspector.GetTogglePersistentMethodInfo(toggle))
                {
                    AddToAction(new TestableAction(collisionCenter.Value, obj, type, mi));
                }
                return;
            }
        }

        bool AddToAction(TestableAction action)
        {
            bool flag = true;
            foreach (var t in testableActions)
            {
                bool A = action.Method == t.Method;
                bool B = action.ConnectedObject == t.ConnectedObject;
                bool C = action.type == t.type;
                bool D = true; //action.ActionType == t.ActionType;

                flag = !(A & B & C & D);
            }

            if (flag)
            {
                //Debug.Log(String.Format("obj: {0} method: {1} type: {2}", action.ConnectedObject, action.Method, action.type));
                testableActions.Add(action);
            }

            return flag;
        }

        void RewriteCode()
        {
            if (!Application.isEditor)
                throw new Exception("This is only for Editor mode!");

            if (!Directory.Exists(TargetPath))
                throw new DirectoryNotFoundException("Target path " + TargetPath + "doesn't exist!");

            if (File.Exists(RestorePath + codeDataFilename))
                throw new Exception("Restore path has " + codeDataFilename + "!\nThis is probably an already modified target.");

            int targetPathLen = TargetPath.Length;

            if (!Directory.Exists(RestorePath)) Directory.CreateDirectory(RestorePath);
            //File.Create(RestorePath + codeDataPath).Close();

            SourceCodeData codeData = new SourceCodeData();
            foreach (string sourceFileName in Directory.EnumerateFiles(TargetPath, "*.cs", SearchOption.AllDirectories))
            {
                string directory = new FileInfo(sourceFileName).DirectoryName;
                if (directory.Contains("Editor") || directory.Contains("UGUITester") || directory.Contains("Plugin"))
                    continue; // Ignore editor and plugin only project(s)

                string destDir = Path.GetDirectoryName(RestorePath + sourceFileName.Remove(0, targetPathLen));
                string relSrcName = sourceFileName.Remove(0, targetPathLen);
                List<SourceCodeData.StatementData> statements = new List<SourceCodeData.StatementData>();
                codeData.DataByFile[relSrcName] = statements;
                if (!Directory.Exists(destDir)) Directory.CreateDirectory(destDir);
                string destFile = RestorePath + relSrcName;
                File.Copy(sourceFileName, destFile);

                SyntaxTree tree = CSharpSyntaxTree.ParseText(File.ReadAllText(sourceFileName));
                CodeModificationResult result = UGUICodeModifier.VisitSyntaxTree(tree, statements, codeData.DataByHash);
                SyntaxNode newRoot = result.newRoot;
                if (result.UGUIinternal) File.Delete(destFile);
                else
                {
                    File.WriteAllText(sourceFileName, newRoot.ToFullString());
                }
            }
            File.WriteAllText(RestorePath + codeDataFilename, JsonConvert.SerializeObject(codeData, Formatting.Indented));
            Debug.Log("Rewriting Complete!");
        }

        void RestoreCode()
        {
            if (!Directory.Exists(RestorePath))
                throw new DirectoryNotFoundException("Restoration path " + RestorePath + "doesn't exist!");

            if (!File.Exists(RestorePath + codeDataFilename))
                throw new Exception("Restoration path doesn't have " + codeDataFilename + ".\nThis is probably not a valid restoration path!");

            int restorePathLen = RestorePath.Length;

            foreach (string sourceFile in Directory.EnumerateFiles(RestorePath, "*.cs", SearchOption.AllDirectories))
            {
                string dest = Path.GetDirectoryName(RestorePath + sourceFile.Remove(0, restorePathLen));
                File.Copy(sourceFile, TargetPath + sourceFile.Remove(0, restorePathLen), true);
                File.Delete(sourceFile);
            }

            File.Delete(RestorePath + codeDataFilename);
            Debug.Log("Restoration Complete!");
        }

        private bool true_stop = false;
        private void OnApplicationQuit()
        {
            if (IsCodeModified)
            {
                WriteExecutionLog();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = !true_stop;
#endif
            }
        }

        public void WriteExecutionLog()
        {
            string CurrentLogPath = ExecutionLogPath + "/" + CurrentLogData.ID;
            Directory.CreateDirectory(ExecutionLogPath);
            Directory.CreateDirectory(CurrentLogPath);
            File.WriteAllText(CurrentLogPath + "/Line Coverage.log", CurrentLogData.GenerateOutputMessage());
            File.WriteAllText(CurrentLogPath + "/Execution Data.json", JsonConvert.SerializeObject(CurrentLogData, Formatting.Indented));

            using (StreamWriter writer = new StreamWriter(ExecutionLogPath + "/" + "Line Coverage.log"))
            {
                writer.WriteLine("Unity GUI Tester Line Coverage Report");
                writer.WriteLine();
                PreviousLogData.Add(CurrentLogData);
                int coveredCount = 0;
                foreach (var kvp in CurrentCodeData.DataByFile)
                {
                    writer.WriteLine(kvp.Key);
                    writer.WriteLine();
                    foreach (SourceCodeData.StatementData statementData in kvp.Value)
                    {
                        string coveredMessage = "NOT COVERED           ";
                        foreach (ExecutionLogData logData in PreviousLogData)
                            if (logData.CoveredStatements.Contains(statementData.hash))
                            {
                                coveredMessage = "COVERED BY " + logData.ID + " ";
                                coveredCount++;
                                break;
                            }
                        writer.WriteLine(coveredMessage + statementData.line + ": " + statementData.content);
                    }
                    writer.WriteLine();
                }
                writer.WriteLine("Total Line Coverage: " + (float)coveredCount / CurrentCodeData.DataByHash.Count * 100 + "%");
                writer.WriteLine("(" + coveredCount + " of " + CurrentCodeData.DataByHash.Count + " lines covered)");
                PreviousLogData.Remove(CurrentLogData);
            }

            Debug.Log("Log written down!");
        }

        void RemoveExecutionLog()
        {
            if (Directory.Exists(ExecutionLogPath))
            {
                Directory.Delete(ExecutionLogPath, true);
                Debug.Log("Done!");
            }
            else
                Debug.LogWarning("Execution log path doesn't exist!");
        }

        #region Legacy
        [Obsolete]
        void CatchInterface(GameObject obj)
        {
            foreach (var t in InterfaceList)
            {
                var b = obj.GetComponent(t);
                if (b != null)
                {
                    Debug.Log(String.Format("{0} {1} {2}", gameObject, b, t));
                    testableInterfaces.Add(new TestableInterface(obj, b.GetType(), null, t));
                }
            }
        }

        [Obsolete]
        void MonoCatch(GameObject obj)
        {
            Vector3? collisionCenter = null;
            Collider2D collider2D = obj.GetComponent<Collider2D>();
            if (collider2D) collisionCenter = collider2D.bounds.center;
            Collider collider = obj.GetComponent<Collider>();
            if (collider) collisionCenter = collider.bounds.center;

            if (collisionCenter == null) return;

            foreach (MonoBehaviour mb in obj.GetComponents<MonoBehaviour>())
            {
                foreach (var kvp in monobehaviourFunctionLink)
                {
                    /// MonoBehaviour에 달린거 보고 함.
                    /// monobehaviourFunctionLink 에 있는 것들을 가져옴.
                    MethodInfo mi = mb.GetMethodInfo(kvp.Key);
                    if (mi != null)
                    {
                        //testableActions.Add(new TestableAction(collisionCenter.Value, kvp.Value, gameObject, mb.GetType(), mi));
                    }
                }
            }
        }

        [Obsolete]
        bool onlyOnce = true;

        [Obsolete]
        float _timer = 0;

        void ButtonSimulOne()
        {
            int wid = Screen.width;
            int hei = Screen.height;

            wid = UnityEngine.Random.Range(0, wid);
            hei = UnityEngine.Random.Range(0, hei);

            ButtonSimulationScript.Instance.ClickPosition(new Vector2(wid, hei));
        }

        void ButtonSimulTwo()
        {
            int count = testableActions.Count;

            int tt = UnityEngine.Random.Range(0, count);

            if (testableActions[tt].ConnectedObject == null)
            {
                testableActions.Remove(testableActions[tt]);
                return;
            }

            ButtonSimulationScript.Instance.ClickObject(testableActions[tt].ConnectedObject);
        }

        [Obsolete]
        IEnumerator Simule()
        {
            if (onlyOnce)
            {
                if (testSequence.Count > 0)
                {
                    onlyOnce = false;
                    //foreach (var a in testableActions)
                    foreach (var a in testSequence)
                    {
                        a.Execute();
                        yield return null;
                    }
                    Debug.Log("Test Finish!");
                    yield return new WaitForSeconds(5);
                    UnityEditor.EditorApplication.isPlaying = false;
                    yield return null;
                }
                else
                {
                    true_stop = true;
                    yield return null;
                }
            }
        }


        #endregion
    }
}