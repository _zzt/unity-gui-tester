﻿using System;
using System.Reflection;
using UnityEngine;

namespace UGUITester
{
    internal class TestableInterface
    {
        public void Execute()
        {
            ButtonSimulationScript.Instance.InvokeHandler(ConnectedObject, interfaceType);
        }

        public readonly GameObject ConnectedObject;
        public readonly Type type;
        public readonly MethodInfo Method;

        public readonly Type interfaceType;


        /// <summary>
        /// 1. 버튼의 어디쯤 클릭해야 하는지 정보
        /// 2. 어떤 액션인지 (클릭 가따데기 등)
        /// 3. 게임오브젝트
        /// </summary>
        /// <param name="worldPos"></param>
        /// <param name="actionType"></param>
        /// <param name="connectedObject"></param>
		public TestableInterface(GameObject obj, Type t, MethodInfo me, Type interfaceType)
        {
            ConnectedObject = obj;
            type = t;
            Method = me;
            this.interfaceType = interfaceType;
        }
    }
}