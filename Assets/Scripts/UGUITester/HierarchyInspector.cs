﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR

using UnityEditor;



namespace UGUITester
{
    public static class HierarchyInspector
    {
        [MenuItem("GUI Tester/What does this red button do?")]
        static void Amon()
        {
        }

        public static void TraverseHierarchy(Action<GameObject> action)
        {
            foreach (GameObject obj in UnityEngine.Object.FindObjectsOfType<GameObject>())
            {
                if (obj.transform.parent == null)
                    VisitObject(obj, action);
            }
        }

        static void VisitObject(GameObject obj, Action<GameObject> action)
        {
            action(obj);

            foreach (Transform transform in obj.transform)
            {
                VisitObject(transform.gameObject, action);
            }
        }


		// We need to refactor this. Send this somewhere else?

        public static MethodInfo[] GetButtonPersistentMethodInfo(Button button)
        {
            if (button == null)
                return new MethodInfo[0];

            List<MethodInfo> methodInfoList = new List<MethodInfo>();

            UnityEvent onClick = button.onClick;
            foreach (object obj in (IEnumerable)onClick.GetField("m_PersistentCalls").GetField("m_Calls"))
            {
                MethodInfo mi = onClick.InvokeMethod("FindMethod", obj) as MethodInfo;
                methodInfoList.Add(mi);
            }

            return methodInfoList.ToArray();
        }
        // OnSelectItem(Toggle)
        // Show()

        public static MethodInfo[] GetDropdownPersistentMethodInfo(Dropdown dropdown)
        {
            if (dropdown == null)
                return new MethodInfo[0];

            List<MethodInfo> methodInfoList = new List<MethodInfo>();

            UnityEvent<int> onValueChanged = dropdown.onValueChanged;
            foreach (object obj in (IEnumerable)onValueChanged.GetField("m_PersistentCalls").GetField("m_Calls"))
            {
                MethodInfo mi = onValueChanged.InvokeMethod("FindMethod", obj) as MethodInfo;
                methodInfoList.Add(mi);
            }

            return methodInfoList.ToArray();
        }

        // Set(float, bool sendCallback)
        // float -> clamp01
        public static MethodInfo[] GetScrollbarPersistentMethodInfo(Scrollbar scrollbar)
        {
            if (scrollbar == null)
                return new MethodInfo[0];

            List<MethodInfo> methodInfoList = new List<MethodInfo>();

            UnityEvent<float> onValueChanged = scrollbar.onValueChanged;
            foreach (object obj in (IEnumerable)onValueChanged.GetField("m_PersistentCalls").GetField("m_Calls"))
            {
                MethodInfo mi = onValueChanged.InvokeMethod("FindMethod", obj) as MethodInfo;
                methodInfoList.Add(mi);
            }

            return methodInfoList.ToArray();
        }

        // Set(float, bool sendCallback)
        // OnDidApplyAnimationProperties 도 있지만 이건 따로 event 만들기 적절지 않다고 봄.
        // normalizedValue 부르면 value 가 min max 안에서 나옴
        public static MethodInfo[] GetSliderPersistentMethodInfo(Slider slider)
        {
            if (slider == null)
                return new MethodInfo[0];

            List<MethodInfo> methodInfoList = new List<MethodInfo>();

            UnityEvent<float> onValueChanged = slider.onValueChanged;
            foreach (object obj in (IEnumerable)onValueChanged.GetField("m_PersistentCalls").GetField("m_Calls"))
            {
                MethodInfo mi = onValueChanged.InvokeMethod("FindMethod", obj) as MethodInfo;
                methodInfoList.Add(mi);
            }

            return methodInfoList.ToArray();
        }

        // Set(bool, sendCallback)
        public static MethodInfo[] GetTogglePersistentMethodInfo(Toggle toggle)
        {
            if (toggle == null)
                return new MethodInfo[0];

            List<MethodInfo> methodInfoList = new List<MethodInfo>();

            UnityEvent<bool> onValueChanged = toggle.onValueChanged;
            foreach (object obj in (IEnumerable)onValueChanged.GetField("m_PersistentCalls").GetField("m_Calls"))
            {
                MethodInfo mi = onValueChanged.InvokeMethod("FindMethod", obj) as MethodInfo;
                methodInfoList.Add(mi);
            }

            return methodInfoList.ToArray();
        }
    }
}

#endif