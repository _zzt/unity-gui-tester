﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UGUITester
{
	/// <summary>
	/// 라인 커버리지를 저장하는 클래스
	/// </summary>
	[Serializable]
	public class ExecutionLogData
    {
        [NonSerialized]
		public readonly SourceCodeData SourceCodeData;

        private int currentCoverage = 0;

        [JsonProperty]
        public Dictionary<long, List<int>> CoveredActions = new Dictionary<long, List<int>>();

        public List<int> CoverageChanged = new List<int>();

        public readonly int ID;

		public ExecutionLogData(SourceCodeData sourceCodeData, int id)
		{
			SourceCodeData = sourceCodeData;
			ID = id;
        }

        /// <summary>
        /// 커버된 해쉬를 기억하고 나중에 커버 되었는지 아닌지 비교함.
        /// </summary>
        [JsonProperty]
		public HashSet<long> CoveredStatements = new HashSet<long>();
		/// <summary>
		/// 이번 로그에서 실행된 애들의 목록
		/// </summary>
		[JsonProperty]
		internal Dictionary<float, TestableAction> ExecutedActionsByTime = new Dictionary<float, TestableAction>();

		public void LogStatementCall(long hash)
		{
			CoveredStatements.Add(hash);
            if (!CoveredActions.ContainsKey(hash))
            {
                CoveredActions.Add(hash, new List<int> { ExecutedActionsByTime.Count - 1 });
            }
            else
            {
                if(!CoveredActions[hash].Contains(ExecutedActionsByTime.Count - 1))
                {
                    CoveredActions[hash].Add(ExecutedActionsByTime.Count - 1);
                }
            }
        }

		public float GetLineCoverage() => (float)CoveredStatements.Count / SourceCodeData.DataByHash.Count;

		public string GenerateOutputMessage()
		{
            SaveCurrentCoverage();
			StringWriter writer = new StringWriter();
			writer.WriteLine("Unity GUI Tester Line Coverage Report");
			writer.WriteLine();
			writer.WriteLine("ID #: " + ID);
			writer.WriteLine();
			foreach (var kvp in SourceCodeData.DataByFile)
			{
				writer.WriteLine(kvp.Key);
				writer.WriteLine();
				foreach (SourceCodeData.StatementData statementData in kvp.Value)
				{
                    //writer.WriteLine((CoveredStatements.Contains(statementData.hash) ? "COVERED   " : "UNCOVERED ") +
                    //	statementData.line + ": " + statementData.content);
                    if (CoveredStatements.Contains(statementData.hash))
                    {
                        writer.WriteLine("COVERED   " +
                            statementData.line +
                            "   AT " +
                            string.Join(", ", CoveredActions[statementData.hash].Select(i => (i == -1) ? "INITIAL" : string.Format("SEQ {0}", i))) +
                            ": " + statementData.content);
                    }
                    else
                    {
                        writer.WriteLine("UNCOVERED " +
                            statementData.line +
                            "                  : " + statementData.content);
                    }
                }
				writer.WriteLine();
			}
			writer.WriteLine("Total Line Coverage: " + GetLineCoverage() * 100 + "%");
			writer.WriteLine("(" + CoveredStatements.Count + " of " + SourceCodeData.DataByHash.Count + " lines covered)");
			return writer.ToString();
		}

        public void SaveCurrentCoverage()
        {
            CoverageChanged.Add(CoveredStatements.Count - currentCoverage);
            currentCoverage = CoveredStatements.Count;
        }
	}
}