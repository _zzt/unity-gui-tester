﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using JetBrains.Annotations;
using System.Runtime.InteropServices;

namespace SampleGUITarget
{

	public class TestScript : MonoBehaviour
	{

		[SerializeField]
		private GameObject objToClick;

		// Use this for initialization
		IEnumerator Start()
		{
			yield return new WaitForSeconds(2);
			ClickObject(objToClick);
		}

		private void Update()
		{
			if (Input.GetKeyUp(KeyCode.A))
			{
				Debug.Log("AAAA");
			}
		}

		public void ClickObject(GameObject obj)
		{
			EventSystem system = FindObjectOfType<EventSystem>();
			StandaloneInputModule module = FindObjectOfType<StandaloneInputModule>();

			PointerEventData eventData = new PointerEventData(system)
			{
				pointerCurrentRaycast = new RaycastResult()
				{
					gameObject = obj,
				},
				eligibleForClick = true,
				button = PointerEventData.InputButton.Left,
				position = gameObject.transform.position,
			};

			ExecuteEvents.EventFunction<IPointerClickHandler> callback = (handler, ed) =>
			{
				handler.OnPointerClick(ExecuteEvents.ValidateEventData<PointerEventData>(ed));
			};

			ExecuteEvents.ExecuteHierarchy<IPointerClickHandler>(obj, eventData, callback);
		}
		public void ClickPosition(Vector2 screenPos)
		{
			EventSystem system = FindObjectOfType<EventSystem>();

			PointerEventData eventData = new PointerEventData(system)
			{
				position = screenPos,
			};

			List<RaycastResult> raycastList = new List<RaycastResult>();
			system.RaycastAll(eventData, raycastList);

			foreach (RaycastResult r in raycastList)
				if (r.gameObject != null)
				{
					ClickObject(r.gameObject);
					break;
				}
		}

		IEnumerator AutoClick()
		{
			// 1초 후 전방에 클릭 발싸!
			yield return new WaitForSeconds(1);

			GameObject target = FindObjectOfType<Text>().gameObject;

			// 둘 중 하나를 주석 해제해서 쓰시오
			// 하나는 GameObject에 날리는 것, 하나는 좌표에 날리는 것.

			// ClickObject(target);
			// ClickPosition(target.transform.position);
		}


		public void YEE()
		{
			FindObjectOfType<Text>().text = "YEE";
		}

		public void TEST(string arg)
		{

		}
	}
}