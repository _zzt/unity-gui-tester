﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SampleGUITarget
{
	public class OldEventInvokerScript : MonoBehaviour
	{
		public UnityEngine.UI.Button RealButton;
		public UnityEngine.UI.Button FakeButton;

		// Use this for initialization
		void Start()
		{
			Debug.Log(FakeButton.onClick.GetPersistentEventCount());
			FakeButton.onClick.AddListener(() => { Debug.Log("YEE"); });
			Debug.Log(FakeButton.onClick.GetPersistentEventCount());
			Debug.Log("");

			Debug.Log(RealButton.onClick.GetPersistentEventCount());
			Debug.Log(RealButton.onClick.GetPersistentMethodName(0));
			Debug.Log(RealButton.onClick.GetPersistentTarget(0));
			//Debug.Log("YEE");
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}