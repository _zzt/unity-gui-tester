﻿using System.Collections;
using System.Collections.Generic;
using UGUITester;
using UnityEngine;

namespace SampleGUITarget
{

	public class MonobehaviourListener : MonoBehaviour
	{

		int x = 0;

		private void OnMouseDrag()
		{
			Debug.Log("YEEE" + ++x);
		}

		IEnumerator Start()
		{
			yield return new WaitForEndOfFrame();
			Debug.Log(Camera.main.WorldToScreenPoint(
				GameObject.FindObjectOfType<MonobehaviourListener>().GetComponent<BoxCollider2D>().bounds.center));

			while (true)
			{
				yield return new WaitForSeconds(1);
				//Debug.Log(Input.mousePosition);
			}
		}

		void Update()
		{
			if (Input.GetKeyUp(KeyCode.B))
			{
				WindowsInputSimulator.MoveCursorToScreenPosition(Camera.main.WorldToScreenPoint(
					GameObject.FindObjectOfType<MonobehaviourListener>().GetComponent<BoxCollider2D>().bounds.center));

				StartCoroutine(PressFor2sec());
			}
		}

		IEnumerator PressFor2sec()
		{
			WindowsInputSimulator.MouseEvent(MouseEvent.MOUSEEVENTF_LEFTDOWN | MouseEvent.MOUSEEVENTF_LEFTUP);
			//yield return new WaitForSeconds(2);
			//WindowsInputSimulator.MouseEvent(MouseEvent.MOUSEEVENTF_LEFTUP);
			yield return null;
		}
	}

}