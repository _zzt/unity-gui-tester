﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            using (System.IO.StreamReader rdr = new System.IO.StreamReader(
                System.IO.File.Open("C:\\Users\\Magun\\Documents\\ConsoleApp1\\ConsoleApp1\\SampleCode.cs",
                System.IO.FileMode.Open)))
            {
                s = rdr.ReadToEnd();
            }
            Console.WriteLine(s);
            //s = @"Sulho(""zzt"", 10);";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(s);

            var root = (CompilationUnitSyntax)tree.GetRoot();
            var collector = new Collector();
            Console.WriteLine("Visit Start!");
            collector.Visit(root);

            Console.WriteLine("Visit Finish!\n");

            foreach(var directive in collector.Usings)
            {
                
                Console.WriteLine(directive.GetText());
            }
            var newFunctionInvocation = SyntaxFactory.ExpressionStatement(
                SyntaxFactory.InvocationExpression(SyntaxFactory.IdentifierName("Sulho"))
                .WithArgumentList(
                    SyntaxFactory.ArgumentList()
                    .WithOpenParenToken(
                        SyntaxFactory.Token(
                            SyntaxKind.OpenParenToken))
                            .WithCloseParenToken(
                        SyntaxFactory.Token(
                            SyntaxKind.CloseParenToken))))
                            .WithSemicolonToken(
                SyntaxFactory.Token(
                    SyntaxKind.SemicolonToken));

            //var new_root = root.InsertNodesAfter(newFunctionInvocation, collector.Usings);
            SyntaxList<StatementSyntax> syntaxList = new SyntaxList<StatementSyntax>(newFunctionInvocation);
            var new_root = root.InsertNodesAfter(((MethodDeclarationSyntax)collector.Usings[0]).Body.Statements[0], syntaxList);

            Console.WriteLine(new_root.ToString());

            Console.ReadLine();

        }
    }

    class Collector : CSharpSyntaxWalker
    {
        public readonly List<SyntaxNode> Usings = new List<SyntaxNode>();
        static int Tabs = 0;
        public override void Visit(SyntaxNode node)
        {
            Tabs++;
            var indents = new String('\t', Tabs);
            Console.WriteLine("visit {0} {1}", indents, node.Kind());
            if(node.Kind() == SyntaxKind.MethodDeclaration)
            {
                //foreach(var n in ((MethodDeclarationSyntax)node).Body.Statements)
                //{
                //    this.Usings.Add(n);
                //}
                this.Usings.Add(node);
                Console.WriteLine("check {0} Name: {1}", indents, ((MethodDeclarationSyntax)node).Identifier.ValueText);

                //var newFunctionInvocation = SyntaxFactory.ExpressionStatement(
                //    SyntaxFactory.InvocationExpression(SyntaxFactory.IdentifierName("Sulho"))
                //    .WithArgumentList(
                //        SyntaxFactory.ArgumentList()
                //        .WithOpenParenToken(
                //            SyntaxFactory.Token(
                //                SyntaxKind.OpenParenToken))
                //                .WithCloseParenToken(
                //            SyntaxFactory.Token(
                //                SyntaxKind.CloseParenToken))))
                //                .WithSemicolonToken(
                //    SyntaxFactory.Token(
                //        SyntaxKind.SemicolonToken));
                //node.Parent.InsertNodesAfter(node, newFunctionInvocation);
                //node.Parent.ReplaceNode(node, ((MethodDeclarationSyntax)node).AddBodyStatements(newFunctionInvocation));
                //node.Parent.InsertNodesAfter(newFunctionInvocation, node.DescendantNodes().OfType<MethodDeclarationSyntax>().Single().add);
                //SyntaxTree tree = CSharpSyntaxTree.ParseText(@"Sulho(""zzt"", 10);");
                //node.Parent.InsertNodesAfter(node, (SyntaxNode)tree.GetRoot().inser);
            }
            //base.VisitUsingDirective(node);
            //if(node.Name.ToString() != "System" && ! node.Name.ToString().StartsWith("System."))
            //{ 
            //    this.Usings.Add(node);
            //}

            //this.Usings.Add(node);
            base.Visit(node);
            Tabs--;
        }
    }
}
